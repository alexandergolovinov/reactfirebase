import React, {Component} from 'react';
import FormError from "./FormError";
import Meeting from "./Meeting";

export default class Meetings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            meetingName: '',
            errorMessage: null
        };
        this.handleMeetingInput = this.handleMeetingInput.bind(this);
    }

    handleMeetingInput = event => {
        let inputName = event.target.name;
        let inputValue = event.target.value;
        this.setState({
            [inputName]: inputValue
        })
    };

    handleMeetingSubmit = event => {
        event.preventDefault();
        if (this.state.meetingName.length < 3) {
            this.setState({
                errorMessage: 'The Meeting Should be at least 3 char long'
            });
        } else {
            //if everything is fine, then push the data. "addMeeting" method is passed from App.js Component
            this.setState({
                errorMessage: null,
                meetingName: '' //empty the input field
            });
            this.props.addMeeting(this.state.meetingName);
        }
    };

    render() {
        const {meetings} = this.props;

        let showError = null;
        if (this.state.errorMessage !== null) {
            showError = <FormError theMessage={this.state.errorMessage}/>;
        } else {
            showError = null
        }

        return (
            <div className="container mt-4">
                <div className="row justify-content-center">
                    <div className="col-md-8 text-center">
                        <h1 className="font-weight-light">Add a Meeting</h1>
                        {showError}
                        <div className="card bg-light">
                            <div className="card-body text-center">
                                <form className="formgroup" onSubmit={this.handleMeetingSubmit}>
                                    <div className="input-group input-group-lg">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="meetingName"
                                            placeholder="Meeting name"
                                            aria-describedby="buttonAdd"
                                            value={this.state.meetingName}
                                            onChange={this.handleMeetingInput}
                                        />
                                        <div className="input-group-append">
                                            <button
                                                type="submit"
                                                className="btn btn-sm btn-info"
                                                id="buttonAdd"
                                            >
                                                +
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="card bg-light my-3">
                            <div className="card-body text-center">
                                <h4 className="card-title font-weight-light mb-3">Your Meetings</h4>
                                {meetings && meetings.length ? (
                                        <div className="list-group list-group-flush text-center">
                                            {meetings.map(meeting =>
                                                <Meeting userId={this.props.userId} key={meeting.uid} meeting={meeting}/>
                                            )}
                                        </div>
                                    )
                                    : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}