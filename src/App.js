import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route
} from "react-router-dom";
import Home from './Home'
import Login from "./Login";
import Register from "./Register";
import Meetings from "./Meetings";
import CheckIn from "./CheckIn";
import './App.css';
import firebase from "./Firebase";
import Navigation from "./Navigation";

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            user: null,
            displayName: null,
            userId: null,
            meetings: []
        }
    }

    //bad approach.. Redirect does not work from "react-router-dom" library
    renderRedirect = url => {
        document.location.href = url;
    };

    addMeetingToDatabase = meetingName => {
        const ref = firebase
            .database()
            .ref(`/meetings/${this.state.userId}`);
        ref.push({meetingName: meetingName});
    };

    componentDidMount() {
        firebase.auth().onAuthStateChanged(firebaseUser => {
            if (firebaseUser != null) {
                this.setState({
                    user: firebaseUser.displayName,
                    displayName: firebaseUser.displayName,
                    userId: firebaseUser.uid
                });

                const meetingsRef = firebase.database().ref("/meetings/"+this.state.userId);
                meetingsRef.on('value', snapshot => {
                    let meetingInDb = snapshot.val();
                    let allMeetings = [];
                    for (let item in meetingInDb) {
                        allMeetings.push({
                            meetingId: item,
                            meetingName: meetingInDb[item].meetingName
                        })
                    }
                    this.setState({meetings: allMeetings});
                })

            } else {
                //Set "User" to null if we cannot get it from the database
                this.setState({user: null})
            }
        })
    }

    registerUserNameInfo = (userName) => {
        firebase.auth().onAuthStateChanged(firebaseUser => {
            firebaseUser.updateProfile({
                displayName: userName
            }).then(() => {
                this.setState({
                    user: firebaseUser,
                    displayName: firebaseUser.displayName,
                    userId: firebaseUser.uid
                });
            })
        })
    };

    logOutUser = event => {
        event.preventDefault();
        this.setState({
            user: null,
            displayName: null,
            userId: null
        });
      firebase.auth().signOut()
          .then(() => this.renderRedirect('/login'))
    };

    render() {
        return (
            <Router path="/">
                <Navigation user={this.state.user} logoutUser={this.logOutUser}/>

                <Route path="/checkin/:userID/:meetingID" component={() => <CheckIn /> }/>
                <Route path="/" exact={true} component={() => <Home userName={this.state.user}/>}/>
                <Route path="/register" component={() => <Register registerUser={this.registerUserNameInfo}/>}/>
                <Route path="/meetings" component={() => <Meetings userId={this.state.userId} meetings={this.state.meetings} addMeeting={this.addMeetingToDatabase}/>} />
                <Route path="/login" component={Login} />
            </Router>
        )
    }
}