import React, {Component} from 'react';
import firebase from "./Firebase";

export default class CheckIn extends Component {

    constructor(props) {
        super(props);
        this.state = {
            displayName: '',
            email: ''
        };
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
    }
    //bad approach.. Redirect does not work from "react-router-dom" library
    renderRedirect = url => {
        document.location.href = url;
    };

    handleSubmit = event => {
        event.preventDefault();
        const ref = firebase
            .database()
            .ref(`meetings/${this.props.userID}/${this.props.meetingID}/attendees`);
        ref.push({
            attendeeName: this.state.displayName,
            attendeeEmail: this.state.email
        }, () => this.renderRedirect(`/attendees/${this.props.userID}/${this.props.meetingID}`))
    };

    handleInput = event => {
        const itemName = event.target.name;
        const itemValue = event.target.value;
        this.setState({
            [itemName]: itemValue
        });
    };

    render() {
        return (
            <form className="mt-3" onSubmit={this.handleSubmit}>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="card bg-light">
                                <div className="card-body">
                                    <h3 className="font-weight-light mb-3">Check in</h3>
                                    <section className="form-group">
                                        <label
                                            className="form-control-label sr-only"
                                            htmlFor="displayName"
                                        >
                                            Name
                                        </label>
                                        <input
                                            required
                                            className="form-control"
                                            type="text"
                                            id="displayName"
                                            name="displayName"
                                            placeholder="Name"
                                            value={this.state.displayName}
                                            onChange={this.handleInput}
                                        />
                                    </section>
                                    <section className="form-group">
                                        <label
                                            className="form-control-label sr-only"
                                            htmlFor="Email"
                                        >
                                            Email
                                        </label>
                                        <input
                                            required
                                            className="form-control"
                                            type="email"
                                            id="email"
                                            name="email"
                                            placeholder="Email"
                                            value={this.state.email}
                                            onChange={this.handleInput}
                                        />
                                    </section>
                                    <div className="form-group text-right mb-0">
                                        <button className="btn btn-primary" type="submit">
                                            Check in
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}