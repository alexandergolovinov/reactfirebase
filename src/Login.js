import React, {Component} from 'react';
import firebase from "./Firebase";
import FormError from "./FormError";

export default class Login extends Component {

    constructor() {
        super();
        this.state = {
            email: null,
            password: null,
            errorMessage: null
        };
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
    }

    //bad approach.. Redirect does not work from "react-router-dom" library
    renderRedirect = url => {
        document.location.href = url;
    };

    handleInput = event => {
        const itemName = event.target.name;
        const itemValue = event.target.value;
        this.setState({
            [itemName]: itemValue
        });
    };

    handleSubmit = event => {
        const userInfo = {
            email: this.state.email,
            password: this.state.password
        };
        event.preventDefault();

        firebase.auth().signInWithEmailAndPassword(userInfo.email, userInfo.password)
            .then(() => this.renderRedirect("/"))
            .catch(error => {
                if (error !== null) {
                    this.setState({errorMessage: error.message})
                } else {
                    this.setState({errorMessage: null})
                }
            });
    };

    render() {
        let showError = null;
        if (this.state.errorMessage !== null) {
            showError = <FormError theMessage={this.state.errorMessage}/>;
        } else {
            showError = null
        }

        return (
            <div className="text-center mt-4">
                <form className="mt-3" onSubmit={this.handleSubmit}>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-6">
                                <div className="card bg-light">
                                    <div className="card-body">
                                        <h3 className="font-weight-light mb-3">Log in</h3>
                                        {showError}
                                        <section className="form-group">
                                            <label
                                                className="form-control-label sr-only"
                                                htmlFor="Email">
                                                Email
                                            </label>
                                            <input
                                                required
                                                className="form-control"
                                                type="email"
                                                id="email"
                                                name="email"
                                                placeholder="Email"
                                                onChange={this.handleInput}
                                            />
                                        </section>
                                        <section className="form-group">
                                            <input
                                                required
                                                className="form-control"
                                                type="password"
                                                name="password"
                                                placeholder="Password"
                                                onChange={this.handleInput}
                                            />
                                        </section>
                                        <div className="form-group text-right mb-0">
                                            <button className="btn btn-primary" type="submit">
                                                Log in
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}