import React, {Component} from 'react';
import FormError from "./FormError";
import firebase from "./Firebase";

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayName: null,
            email: null,
            passOne: null,
            passTwo: null,
            errorMessage: null
        };
        // Re-define "this" meaning inside local class
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeInput = (event) => {
        const itemName = event.target.name;
        const itemValue = event.target.value;
        this.setState({
                [itemName]: itemValue,
            },
            () => {
                if (this.state.passOne !== this.state.passTwo) {
                    this.setState({errorMessage: 'Passwords do not match'})
                } else {
                    this.setState({errorMessage: null})
                }
            });
        //you can check the "state" changes from the browser console.
        console.log(this.state);
    };

    handleSubmit = (event) => {
        let registrationInfo = {
            displayName: this.state.displayName,
            email: this.state.email,
            passOne: this.state.passOne
        };
        event.preventDefault();

        firebase.auth().createUserWithEmailAndPassword(registrationInfo.email, registrationInfo.passOne)
            .then(() => {
                //this prop "register user" is now available through the constructor(props) defined at the top
                this.props.registerUser(registrationInfo.displayName);
            })
            .catch(error => {
                if (error.message !== null) {
                    // show error message
                    this.setState({
                        errorMessage: error.message
                    })
                } else {
                    // if no errors, then empty the fields
                    this.setState({
                            errorMessage: null
                        }
                    );
                }
            });
    };

    /*input field names attribute help to catch the values*/
    render() {
        let showError = null;
        if (this.state.errorMessage !== null) {
            showError = <FormError theMessage={this.state.errorMessage}/>;
        } else {
            showError = null
        }

        return (
            <form className="text-center mt-3" onSubmit={this.handleSubmit}>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="card bg-light">
                                <div className="card-body">
                                    <h3 className="font-weight-light mb-3">Register</h3>
                                    <div className="form-row">
                                        {showError}
                                        <section className="col-sm-12 form-group">
                                            <label
                                                className="form-control-label sr-only"
                                                htmlFor="displayName"
                                            >
                                                Display Name
                                            </label>
                                            <input
                                                className="form-control"
                                                type="text"
                                                id="displayName"
                                                placeholder="Display Name"
                                                name="displayName"
                                                value={this.state.displayName}
                                                onChange={this.handleChangeInput}
                                                required
                                            />
                                        </section>
                                    </div>
                                    <section className="form-group">
                                        <label
                                            className="form-control-label sr-only"
                                            htmlFor="email"
                                        >
                                            Email
                                        </label>
                                        <input
                                            className="form-control"
                                            type="email"
                                            id="email"
                                            placeholder="Email Address"
                                            required
                                            name="email"
                                            onChange={this.handleChangeInput}
                                            value={this.state.email}
                                        />
                                    </section>
                                    <div className="form-row">
                                        <section className="col-sm-6 form-group">
                                            <input
                                                className="form-control"
                                                type="password"
                                                name="passOne"
                                                placeholder="Password"
                                                onChange={this.handleChangeInput}
                                                value={this.state.passOne}
                                            />
                                        </section>
                                        <section className="col-sm-6 form-group">
                                            <input
                                                className="form-control"
                                                type="password"
                                                required
                                                name="passTwo"
                                                placeholder="Repeat Password"
                                                onChange={this.handleChangeInput}
                                                value={this.state.passTwo}
                                            />
                                        </section>
                                    </div>
                                    <div className="form-group text-right mb-0">
                                        <button className="btn btn-primary" type="submit">
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}