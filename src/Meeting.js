import React, {Component} from 'react';
import firebase from "./Firebase";
import {GoTrashcan} from "react-icons/go";
import {FaLink} from "react-icons/fa";

export default class Meeting extends Component{
    constructor(props) {
        super(props);
        this.deleteMeeting = this.deleteMeeting.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
    }

    //bad approach.. Redirect does not work from "react-router-dom" library
    renderRedirect = url => {
        document.location.href = url;
    };

    deleteMeeting = (event, meetingId) => {
        event.preventDefault();

        const referenceItem = firebase.database().ref(`meetings/${this.props.userId}/${meetingId}`);
        referenceItem.remove()
            .catch(error => {
                console.log('Cannot remove item, ' + error.message + '. ID: ' + meetingId);
            });
    };

    render() {
        const {meeting} = this.props;
        return (
            <div className="list-group-item d-flex">
                <section className="col-8 pl-3 text-left">
                    {meeting.meetingName}
                </section>
                <section className="col-4 btn-group align-self-lg-end" role="group" aria-label="Meeting Options">
                    <button className="btn btn-sm btn-outline-secondary" title="Delete"
                            onClick={e => this.deleteMeeting(e, meeting.meetingId)}>
                        <GoTrashcan/>
                    </button>
                    <button className="btn btn-sm btn-outline-secondary" title="Check In"
                            onClick={() => this.renderRedirect(`/checkin/${this.props.userId}/${meeting.meetingId}`)}>
                        <FaLink/>
                    </button>
                </section>
            </div>
        )
    }
}